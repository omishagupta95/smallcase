FROM python:3

COPY . /app

WORKDIR /app

EXPOSE 5000

RUN pip3 install -r requirements.txt

WORKDIR /app/src

ENTRYPOINT ["python3", "app.py"]
