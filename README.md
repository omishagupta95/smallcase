# Smallcase Assignment

### Architecture Diagram

<div style="text-align:center"><img src="./images/architecture-smallcase.png?raw=true." /></div>

### Features:

- [x] CI/CD pipeline with [GitLab CI](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/).
- [x] Auto deploy on new commits to `master`.
- [x] Create a Docker image.
- [x] Deploy the app to GKE.
- [x] Deploy with blue-green strategy.

### Code Flow: 

<div style="text-align:center"><img src="./images/codeflow-smallcase.png?raw=true." /></div>

### Build steps to install the dependencies.

#### Platforms

- GitLab
- GitLab Runner (self managed)
- Google Kubernetes Engine

GitLab runner is self managed on GCE with following config:
- Google Compute Engine: n1-standard-2, ubuntu 18.04
- SSH keys for GitLab, Authorization for Kubernetes cluster
- Installed packages like [Docker](https://docs.docker.com/engine/install/ubuntu/), [envsubst](https://command-not-found.com/envsubst).

### Directory Structure

```
├── Dockerfile
├── README.md
├── build.sh
├── deployments
│   ├── app.yml
│   └── service.yml
├── images
│   ├── architecture-smallcase.png
│   └── codeflow-smallcase.png
├── requirements.txt
└── src
    ├── app.py
    └── templates
        └── index.html
```