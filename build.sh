#!/bin/bash
set -x

export KUBECONFIG=/home/gitlab-runner/admin.conf 

DEPLOYMENT_BLUE="$APP_NAME-blue"
DEPLOYMENT_GREEN="$APP_NAME-green"

create_docker_image() {
    cd $CI_PROJECT_DIR
    docker build -t "$DOCKER_IMAGE:$VERSION" .
    docker push "$DOCKER_IMAGE:$VERSION"
}

deploy_to_kubernetes() {
    cd $CI_PROJECT_DIR

    CURRENT_DEPLOYMENT=`kubectl get services front-lb --output jsonpath='{.spec.selector.app}'`
    
    if [[ $CURRENT_DEPLOYMENT == *green ]]; then
        NEW_DEPLOYMENT=blue
        # Create a deployment with new version
        NAME="$DEPLOYMENT_BLUE" \
        DOCKER_IMAGE="$DOCKER_IMAGE" \
        PORT=$PORT \
        VERSION=$VERSION \
        envsubst < ./deployments/app.yml | kubectl apply -f -
        
        # Points new deployment to internal load balancer
        NAME="internal-lb" \
        APP_SELECTOR="$DEPLOYMENT_BLUE" \
        PORT=$PORT \
        envsubst < ./deployments/service.yml | kubectl apply -f -
    else
        NEW_DEPLOYMENT=green
        # Create a deployment with new version
        NAME="$DEPLOYMENT_GREEN" \
        DOCKER_IMAGE="$DOCKER_IMAGE" \
        PORT=$PORT \
        VERSION=$VERSION \
        envsubst < ./deployments/app.yml | kubectl apply -f -

        # Points new deployment to internal load balancer
        NAME="internal-lb" \
        APP_SELECTOR="$DEPLOYMENT_GREEN" \
        PORT=$PORT \
        envsubst < ./deployments/service.yml | kubectl apply -f -
    fi

    # Wait till internal load balancer gets IP
    while [[ -z `kubectl get services internal-lb --output jsonpath='{.status.loadBalancer.ingress[0].ip}'` ]]; do 
        sleep 5s
    done

    INTERNAL_LB_IP=`kubectl get services internal-lb --output jsonpath='{.status.loadBalancer.ingress[0].ip}'`

    echo "Internal deployment done at $INTERNAL_LB_IP:$PORT"
    
    # Checks if the pods are rotated:
    # Tries for 30 secs, as default pod rotation timeout = 30s
    COUNT=0
    while [[ `curl -LI $INTERNAL_LB_IP:$PORT/health -o /dev/null -w '%{http_code}\n' -s` == 000 ]]; do 
        sleep 5s
        COUNT=$((COUNT + 1))
        # Check if it has taken more than 30s
        if [[ $COUNT == 6 ]]; then
            echo "The deployment is returning code 000, consider relooking the deployment"
            exit 1
        fi
    done

    RESPONSE_CODE=`curl -LI $INTERNAL_LB_IP:$PORT/health -o /dev/null -w '%{http_code}\n' -s`
    
    # Test new deployment:
    # For simplicity, check if it is able to serve traffic
    if [[ $RESPONSE_CODE =~ 20* ]]; then
        if [[ $NEW_DEPLOYMENT == blue ]]; then
            # Point front load balancer to new version
            NAME="front-lb" \
            APP_SELECTOR="$DEPLOYMENT_BLUE" \
            PORT=$PORT \
            envsubst < ./deployments/service.yml | kubectl apply -f -
            
            # Point internal load balancer to old version
            NAME="internal-lb" \
            APP_SELECTOR="$DEPLOYMENT_GREEN" \
            PORT=$PORT \
            envsubst < ./deployments/service.yml | kubectl apply -f -
        else
            # Point front load balancer to new version
            NAME="front-lb" \
            APP_SELECTOR="$DEPLOYMENT_GREEN" \
            PORT=$PORT \
            envsubst < ./deployments/service.yml | kubectl apply -f -
            
            # Point internal load balancer to old version
            NAME="internal-lb" \
            APP_SELECTOR="$DEPLOYMENT_BLUE" \
            PORT=$PORT \
            envsubst < ./deployments/service.yml | kubectl apply -f -
        fi

        # Checks if front lb has got the IP assigned
        while [[ -z `kubectl get services front-lb --output jsonpath='{.status.loadBalancer.ingress[0].ip}'` ]]; do 
            sleep 5s
        done

        FRONT_LB_IP=`kubectl get services front-lb --output jsonpath='{.status.loadBalancer.ingress[0].ip}'`
        echo "The prod has deployment - $APP_NAME-$NEW_DEPLOYMENT deployed at $FRONT_LB_IP:$PORT"
    else
        echo "The deployments are unhealthy."
    fi
}

if declare -f "$1" > /dev/null
then
# call arguments verbatim
    "$@"
else
# Show a helpful error
    echo "'$1' is not a known function name" >&2
    exit 1
fi
